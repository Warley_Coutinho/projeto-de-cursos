<?php
/* Classe  */
class ContaBanco
{
  public $numConta;
  public $agencia;
  protected $tipo;
  private $dono;
  private $saldo;
  private $status;

  /* Atributos  */
  public function abrirConta($t)
  {
    $this->setTipo($t);
    $this->setStatus(true);
    if ($t == "cc") {
      $this->setSaldo(50);
    } elseif ($t == "cp") {
      $this->setSaldo(150);
    }
  }
  public function fecharConta()
  {
    if($this -> getSaldo() > 0){
      echo"<p>Conta com Saldo Possitivo.Não pode fechada. </p>";
    }elseif ($this -> getSaldo() < 0) {
      echo"<p>Conta em debito</p>";
    } else {
      $this -> setStatus(false);
      echo"<p>Conta de => {$this -> getDono()} fechado com sucesso !!!</p>";
    }
    
  }

  public function depositar($v){
  
     if ($this -> getStatus() ){
     $this -> setSaldo($this -> getSaldo() + $v) ;
     echo"<p>Deposito de R$:$v  na conta do => {$this -> getDono()}!!!</p>";
  }else {
    echo"<p>Impossivel depositar</p>";
  }
  }
  
  public function sacar($v){
      if ($this -> getStatus()) {
          if ($this -> getSaldo() >= $v) {
              $this -> setSaldo($this -> getSaldo() - $v);
              echo"<p>Saque de R$:$v autorizado na conta do => {$this -> getDono()}!!!</p>";
          } else {
              echo"<p> Saldo Insuficiente</p>";
          }
         
      }else{
        echo"<p> Impossivel realizar saques</p>";
      }
     
  }

  public function pagaMensal(){ 
    if($this->getTipo()== "cc"){
      $v = 12;
    }elseif($this -> getTipo() == "cp"){
      $v = 20;
    }
    if ($this -> getStatus()){
      $this -> setSaldo($this -> getSaldo() - $v);
      echo"<p>Mensalidade de R$:$v  debitada na conta do => {$this -> getDono()}!!!</p>";
    }else{
      echo"<p> Poblemas com a conta.Não posso cobrar</p>";
    }
      
  }

  // Metodos Especias

  function __construct()// Metodo Contrutor
  {
    $this -> setSaldo(0);
    $this -> setStatus(false);
    echo"<p>conta criada com sucesso!</p>";
  }   
  

  // Metado Especias continua abaixo
  function getNumConta()
  {
    return $this->numConta;
  }

  function setNumConta($numConta)
  {
    $this->numConta = $numConta;
  }
  function getAgencia(){
    return $this -> agencia;
  }
  function setAgencia($agencia){
   $this-> agencia = $agencia;
}

  function getTipo()
  {
    return $this->tipo;
  }
  function  setTipo($tipo)
  {
    $this->tipo = $tipo;
  }
  function getDono()
  {
    return $this->dono;
  }
  function setDono($dono)
  {
    $this->dono = $dono;
  }

  function getSaldo()
  {
    return $this->saldo;
  }

  function setSaldo($saldo)
  {
    $this->saldo = $saldo;
  }

  function getStatus()
  {
    return $this->status;
  }

  function setStatus($status)
  {
    $this->status = $status;
  }
}
