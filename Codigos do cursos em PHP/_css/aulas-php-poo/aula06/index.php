<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="_css/estilo.css"/>
  <meta charset="UTF-8"/>
  <title> Pilares do Encapsulamento </title>
</head>
<body>
<div>
  <pre>
    <?php
    /* Aqui foi declaro o corpo principal da classe Caneta e com o comando
    reuire_once fez a ligação da classe  com o index.php */
       require_once 'ControleRemoto.php' ;
      
       $c = new ControleRemoto;
       $c -> ligar();
      // $c -> desligar();
     // $c -> desligarMudo();
        $c -> abrirMenu();
       //$c -> fecharMenu();
       $c -> maisVolume(70);
     //  $c -> menosVolume(10);
         $c -> play();
        // $c -> pause();
    
       
      print_r($c);
      
    ?>
    </pre>
</div>
</body>
</html>
 