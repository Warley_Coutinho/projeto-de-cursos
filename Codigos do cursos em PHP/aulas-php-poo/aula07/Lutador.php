<?php

/* Classe  */
class Lutador
{
  private $nome;
  private $naciolidade;
  private $idade;
  private $altura;
  private $peso;
  private $categoria;
  private $vitorias;
  private $derrotas;
  private $empates;

  //  Metodo contrutor
  public function __construct($no,$na,$id,$al,$pe,$vi,$de,$em)//passando paramentos
  {
    $this -> nome = $no;
    $this -> nacionalidade = $na;
    $this -> idade = $id;
    $this -> altura = $al;
    $this -> peso = $pe;
    $this -> vitorias = $vi;
    $this -> derrotas = $de;
    $this -> empates = $em;

  }

  // metodo especial getter e setter

  public function getNome()
  {
    return $this->nome;
  }

  public function setNome($nome)
  {
    $this->nome = $nome;
  }

  public function getNaciolidade()
  {
    return $this->naciolidade;
  }

  public function setNaciolidade($naciolidade)
  {
    $this->naciolidade = $naciolidade;
  }

  public function getIdade()
  {
    return $this->idade;
  }

  public function setIdade($idade)
  {
    $this->idade = $idade;
  }

  public function getAltura()
  {
    return $this->altura;
  }

  public function setAltura($altura)
  {
    $this->altura = $altura;
  }

  public function getPeso()
  {
    return $this->peso;
  }

  public function setPeso($peso)
  {
    $this->peso = $peso;
    $this->setCategoria();
  }

  public function getCategoria()
  {
    return $this->categoria;
  }

  public function setCategoria(){

    if($this -> peso < 52.2){
    $this -> categoria = "Invalido";
  }elseif($this -> peso <= 70.3 ){
    $this -> categoria = "Leve";
  }elseif($this -> peso <= 83.9){
     $this -> categoria = "Médio";
  }elseif($this -> peso <= 120.2){
    $this -> categoria = "Pesado";
  }
  $this -> categoria = "Ivalido";
}
  public function getVitorias()
  {
    return $this->vitorias;
  }

  public function setVitorias($vitorias)
  {
    $this->vitorias = $vitorias;
  }

  public function getDerrotas()
  {
    return $this->derrotas;
  }

  public function setDerrotas($derrotas)
  {
    $this->derrotas = $derrotas;
  }

  public function getEmpates()
  {
    return $this->empates;
  }

  public function setEmpates($empates)
  {
    $this->empates = $empates;
  }
  // Metodos abstract atributos
  function apresentar(){

  }
  function status(){

  }
  function ganharLuta(){

  }
  function perderLuta(){

  }
  function empatar(){

  }
}
