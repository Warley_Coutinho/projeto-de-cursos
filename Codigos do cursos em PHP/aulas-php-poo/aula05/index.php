<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="_css/estilo.css"/>
  <meta charset="UTF-8"/>
  <title> Exemplo Prático com objetos </title>
</head>
<body>
<div>
  <pre>
    <?php
    /* Aqui foi declaro o corpo principal da classe Caneta e com o comando
    reuire_once fez a ligação da classe  com o index.php */
       require_once 'ContaBanco.php' ;
       
       $p1 = new ContaBanco;
       $p1 -> setNumConta(15257);// Melhor Metodo de fazer!!!
       $p1 -> setAgencia(2289);// Melhor Metodo de fazer!!!
       $p1 -> abrirConta("cc");
       $p1 -> setDono("Warley");
       $p1 -> depositar(300);
       $p1 -> sacar(338);
       $p1 -> pagaMensal();
       $p1 -> fecharConta();
       print_r($p1);
       echo"<br/>";
       $p2 = new ContaBanco;
       $p2 -> numConta = 15258;//funciona mais não recomendado!
       $p2 -> agencia = 2290;//funciona mais não recomendado!
       $p2 -> abrirConta("cp");
       $p2 -> setDono("Cleude");
       $p2 -> depositar(500);
       $p2 -> sacar(338);
       $p2 -> pagaMensal();
       $p2 -> fecharConta();
       print_r($p2);
      
      
    ?>
    </pre>
</div>
</body>
</html>
 