<?php
/* Aqui foi declaro uma classe  */
class Caneta {
    private $modelo;
    private $cor;
    private $ponta ;
    private $tampada;

  /*function __construct() 
  // Metado construtor sem paramento
  {
    $this -> cor = "azul";
    $this -> tampar();
  }*/

// Metodo Construtor passando paramento 
function __construct($m,$c,$p)
{
  $this -> setModelo($m);//usando o metodo especial
  //$this -> modelo = $m ; // usando sem metodo especial
  $this -> cor = $c;
  $this -> setPonta($p);
  $this -> tampar();
}



  // Metodo  normmal de atribuçoes
  public function tampar(){
    $this -> tampada = true;
  }

   

    // Metodos especias
    public function getModelo()
    {
        return $this -> modelo;
    }
    public function setModelo($m)
    {
        $this -> modelo = $m;
    }

    public function getPonta()
    {
        return $this -> ponta;
    }

    public function setPonta($p)
    {
        $this -> ponta = $p;
    }
}